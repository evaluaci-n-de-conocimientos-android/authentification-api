<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


/*                          BY RAUL PECH
 * --------------------------------------------------------------------
 * Mediante el router que tiene por defecto lumen, se implementa el Endpoint 
 * que sera de tipo POST, esto retornara lo que contenga el controlador
 * dentro de la funcion getToken
 * --------------------------------------------------------------------
 */
$router->post('/aquaculturemobile/ozelot/it/api/login',
['uses'=>'UsersController@getToken']);
$router->get('/', function () use ($router) {
    return $router->app->version();
});

/*                          BY RAUL PECH
 * --------------------------------------------------------------------
 * Estas rutas se utilizan para ver a los usuarios en la base de datos.
 * La ruta get /users y la ruta post /users crea un usuario 
 * --------------------------------------------------------------------
 */
$router->get('/users',
['uses'=>'UsersController@index']);
$router->post('/users',
['uses'=>'UsersController@create']);

