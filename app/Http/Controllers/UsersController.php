<?php

namespace App\Http\Controllers;
Use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersController extends Controller
{

    /*                                   BY RAUL PECH
         * --------------------------------------------------------------------
         * Se declara una variable que contendra
         * todos los usuarios que se ingresaron en la base de datos
         * y los retornara en formato json mediante el metodo index
         * --------------------------------------------------------------------
         */

    function index(Request $request){

        if($request->isJson()){
        //Activacion de eloquen en el archivo app.php
        $user=User::all();
        return response()->json($user,200);
        }
        return response()->json(['error'=>'Unauthorized'],401,[]);
    }

    /*                                   BY RAUL PECH
         * --------------------------------------------------------------------
         * Se crea un metodo create para poder crear un usuario y de esta forma
         * poderle asignar un token al ingresarse a la base de datos, 
         * el password se encripta por el metodo de hashing de lumen para que  
         * no se manipule en texto plano 
         * --------------------------------------------------------------------
         */
    function create(Request $request){
        if($request->isJson()){
            $data=$request->json()->all();   
            $user=User::create([
                'name'=>$data['name'],
                'username'=>$data['username'],
                'email'=>$data['email'],
                /**                          BY RAUL PECH
                 * --------------------------------------------------------------
                 * Activacion de las facades en el archivo app.php e importacion 
                 * del hash para encriptar la contraseña
                 * --------------------------------------------------------------
                 * */
                'password'=>Hash::make($data['password']),
                'api_token'=>Str::random(60)
            ]);

            
             return response()->json([$user],201);
            
            }
            return response()->json(['error'=>'Unauthorized'],401,[]);
    }

            /**                          BY RAUL PECH
             * --------------------------------------------------------------
             * En este metodo otenemos el tokem de verificacion del usuario 
             * de esta forma se valida que el usuario sea el correcto junto al
             * password
             * --------------------------------------------------------------
             * */
            function getToken(Request $request){
                
                if($request->isJson()){
                    try{
                        $data=$request->json()->all(); 
                        $user=User::where('email',$data['email'])->first();
                        //Validacion de la contraseña de parte de la base de datos a la que se envia por post
                        if($user && Hash::check($data['password'],$user->password)){
                            return response()->json($user,200);
                        }else{
                            return response()->json(['error'=>'invalid password or email'],406);
                        }
                    }catch(ModelNotFoundException $e){
                        return response()->json(['error'=>'no content'],406);
                    }
                }
                return response()->json(['error'=>'Unauthorized'],401,[]);
            }

}
